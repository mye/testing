use Mix.Config
config :logger, level: :debug

config :mnesia,
  dir: '.mnesia/#{Mix.env}/#{node()}'

config :tagger,
  token: "testing"
