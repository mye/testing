defmodule TaggerTest do
  use ExUnit.Case, async: true
  doctest Tagger.Storage

  use Tesla
  plug Tesla.Middleware.BaseUrl, "http://127.0.0.1:8880"
  plug Tesla.Middleware.JSON
  plug Tesla.Middleware.Query, [envkind: "sandbox"]

  setup  do
    assert Mix.env == :test
    {:atomic, :ok}  = :mnesia.clear_table(Tagger.Storage)
    :ok
  end

  setup context do
    if Map.has_key?(context, :postme) do
      data = %Tagger.Storage{envkind: "sandbox", name: "imagename", namespace: "xxx", tag: "sometag"}
      Tagger.Storage.post(data)
    end
    :ok
  end

  test "Storage.getall" do
    assert Tagger.Storage.getall() == []
  end

  @tag postme: "fixtures"
  test "Storage.getall postme" do
    assert length(Tagger.Storage.getall()) == 1
  end

  test "Storage.post" do
    data = %Tagger.Storage{envkind: "sandbox", name: "image", namespace: "bot", tag: "sometag2"}
    Tagger.Storage.post(data)
  end

  test "posting data twice" do
    data = %{envkind: "sandbox", name: "image", namespace: "bot", tag: "blabla", token: "testing"}
    {:ok, response} = post("/images", data)
    assert response.status == 201
    {:ok, response2} = post("/images", data)
    assert response2.status == 201
    assert length(Tagger.Storage.getall()) == 1
  end

  test "check web getall" do
    {:ok, response} = get("/getall")
    assert response.status == 200
  end

  @tag postme: "fixtures"
  test "check web get by envkind" do
    {:ok, response} = get("/images")
    assert response.status == 200
  end

  @tag postme: "fixtures"
  test "check web get by envkind and namespace" do
    {:ok, response} = get("/images", query: [namespace: "xxx"])
    assert response.status == 200
  end

  @tag postme: "fixtures"
  test "check web get by envkind and namespace and name" do
    {:ok, response} = get("/images", query: [namespace: "xxx", name: "imagename"])
    assert response.status == 200
  end


  @tag postme: "fixtures"
  test "check web get by envkind and namespace and name non-exist" do
    {:ok, response} = get("/images", query: [namespace: "xxx", name: "imagename2"])
    assert response.status == 404
  end

  @tag postme: "fixtures"
  test "check web non exist get by envkind and namespace" do
    {:ok, response} = get("/images", query: [namespace: "not-exist"])
    assert response.status == 404
  end

  @tag postme: "fixtures"
  test "check web delete method without token" do
    {:ok, response} = delete("/images", query: [id: 1])
    assert response.status == 500
  end

  @tag postme: "fixtures"
  test "check web delete method, twice" do
    {:ok, response} = delete("/images", query: [id: 1, token: "testing"])
    assert response.status == 202
    {:ok, response2} = delete("/images", query: [id: 1, token: "testing"])
    assert response2.status == 202
  end

  @tag postme: "fixtures"
  test "check web delete method, another token" do
    {:ok, response} = delete("/images", query: [id: 1000, token: "testing"])
    assert response.status == 202
  end

  test "check web non exist" do
    {:ok, response} = get("/nonexist")
    assert response.status == 500
  end
end
