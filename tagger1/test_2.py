import requests
import json
import pytest

url = 'http://127.0.0.1:8880'


def test_check_web_getall():
    response = requests.get(url + '/getall')
    print(f'\nresponse.status_code = {response.status_code}')
    assert response.status_code == 200


def test_storage_getall():
    response = requests.get(url + '/getall')
    print(f'\nresponse.status_code = {response.status_code}')
    print(f'response.json() = {response.json()}')
    assert response.json() == []


def test_storage_post():
    new_object = requests.post("http://127.0.0.1:8880/images",
                               data={'name': 'myimage',
                                     'tag': 2,
                                     'namespace': 'test',
                                     'envkind': 'sandbox',
                                     'token': 'testing'})

    assert new_object.status_code == 201


def test_posting_data_twice():
    data = {'name': 'myimage', 'tag': 2, 'namespace': 'test', 'envkind': 'sandbox', 'token': 'testing'}

    new_object = requests.post("http://127.0.0.1:8880/images", data=data)
    assert new_object.status_code == 201

    new_object2 = requests.post("http://127.0.0.1:8880/images", data=data)
    assert new_object2.status_code == 201

    response = requests.get(url + '/getall')
    assert len(response.json()) == 1


def test_check_getall():
    response = requests.get(url + '/getall')
    print(f'response.json() = {response.json()}')
    assert len(response.json()) == 1


def test_search_object0():
    '''check web get by envkind and namespace and name'''
    searech_object = requests.get(url + '/getall')
    expected_object = {'envkind': 'sandbox', 'id': 1, 'name': 'myimage', 'namespace': 'test', 'tag': '2'}
    assert searech_object.json()[0] == expected_object


def test_search_object1():
    payload = {'envkind': 'sandbox'}
    search_object = requests.get("http://127.0.0.1:8880/images", params=payload)
    print(f'\nsearech_object.status_code = {search_object.status_code}')
    assert search_object.status_code == 200


def test_delete_object0():
    ''' Check web delete method without token '''
    payload = {'id': 1}
    delete_object = requests.delete("http://127.0.0.1:8880/images", data=payload)
    print(f'\ndelete_object.status_code = {delete_object.status_code}')
    assert delete_object.status_code == 500


def test_delete_object():
    payload = {'id': 1, 'token': 'testing'}

    delete_object = requests.delete("http://127.0.0.1:8880/images", data=payload)
    print(f'\ndelete_object.status_code = {delete_object.status_code}')
    assert delete_object.status_code == 202

    delete_object2 = requests.delete("http://127.0.0.1:8880/images", data=payload)
    print(f'\ndelete_object2.status_code = {delete_object2.status_code}')
    assert delete_object.status_code == 202

    response = requests.get(url + '/getall')
    print(f'response.text = {response.text}')
    print(f'response.json() = {response.json()}')


def test_check_an_object():
    response = requests.get(url + '/getall')
    print(f'\nresponse.status_code = {response.status_code}')
    assert len(response.json()) == 0


def test_web_non_exist():
    response = requests.get(url + '/nonexist')
    assert response.status_code == 500
