import pytest

# def setup():
#     '''run third'''
#     print("\n222222222222222222222222222222222222222222222222222222222222222222222222222222222 basic setup into module")
#
#
# def teardown():
#     '''always executed AFTER test'''
#     print("\n333333333333333333333333333333333333333333333333333333333333333333333333333333 basic teardown into module")
#
#
# def setup_module(module):
#     '''always executed first'''
#     print("\n00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 module setup")
#
#
# def teardown_module(module):
#     '''always executed in the end'''
#     print("-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1 module teardown")
#
#
# def setup_function(function):
#     '''run second'''
#     print("\n111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 function setup")
#
#
# def teardown_function(function):
#     '''always executed after first test'''
#     print("-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2 function teardown")
#
#
# def test_numbers_3_4():
#     print("test 3*4")
#     assert 3 * 4 == 12
#
#
# def test_strings_a_3():
#     print("test a*3")
#     assert 'a' * 3 == 'aaa'


# class TestUM:
#     def setup(self):
#         print("22222222222222222222222222222222222222222222222222222222222222222222222222222222 basic setup into class")
#
#     def teardown(self):
#         print("\n333333333333333333333333333333333333333333333333333333333333333333333333333 basic teardown into class")
#
#     def setup_class(cls):
#         '''always executed first'''
#         print("\n00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 class setup")
#
#     def teardown_class(cls):
#         print("-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1 class teardown")
#
#     def setup_method(self, method):
#         print("\n1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 method setup")
#
#     def teardown_method(self, method):
#         print("444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 method teardown")
#
#     def test_numbers_5_6(self):
#         print("test 5*6")
#         assert 5 * 6 == 30
#
#     def test_strings_b_2(self):
#         print("test b*2")
#         assert 'b' * 2 == 'bb'



'''
Как было сказано выше, если необходим вызов teardown для определенной расширенной фикстуры, то можно реализовать его 
двумя способами:
1) добавив в фикстуру финализатор (через метод addfinalizer объекта request передаваемого в фикстуру
2) через использование конструкции yield (начиная с PyTest версии 2.4)
Первый способ мы рассмотрели в примере создания расширенной фикстуры. Теперь же просто продемонстрируем ту же самую 
функциональность через использование yield. Следует заметить, что для использования yield при декорировании функции,
как фикстуры, необходимо использовать декоратор @pytest.yield_fixture(), а не @pytest.fixture():
'''

# @pytest.yield_fixture()
# @pytest.fixture()
# def resource_setup():
#     print("\n000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 resource_setup")
#     yield
#     print("\n111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 resource_teardown")


# def test_0(resource_setup):
#     print("TEST_0")


# def test_1():
#     print("\nTEST_1")


# def test_2(resource_setup):
#     print("TEST_2")


class TestUM1:
    def setup(self):
        print("22222222222222222222222222222222222222222222222222222222222222222222222222222222 basic setup into class")

    @pytest.fixture()
    def resource_setup(self):
        print("\n-------------------------------------------------------------------------------------------------------------------- 1 resource_setup")
        yield
        print("\n-------------------------------------------------------------------------------------------------------------------- 2 resource_teardown")

    def teardown(self):
        print("\n333333333333333333333333333333333333333333333333333333333333333333333333333 basic teardown into class")

    def setup_class(cls):
        '''always executed first'''
        print("\n00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 class setup")

    def teardown_class(cls):
        print("\n-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1-1 class teardown")

    def setup_method(self, method):
        print("\n1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 method setup")

    def teardown_method(self, method):
        print("444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 method teardown")

    def test_numbers_5_6(self, resource_setup):
        print("test 5*6")
        assert 5 * 6 == 300

    def test_strings_b_2(self):
        print("test b*2")
        assert 'b' * 2 == 'bb'


'''
 except Exception as ass:

        current_failure_of_test = f'{ass}.png'.replace(' ', '_')
        driver.save_screenshot(os.path.join(os.path.dirname(os.path.realpath(__file__)), './screenshot_failure_of_test',
                                            current_failure_of_test))
    else:
        driver.quit()
'''
#
# class TestMyClass:
#     @classmethod
#     def setup_class(cls):
#         print ('\nsetup_class()')
#
#     @classmethod
#     def teardown_class(cls):
#         print ('teardown_class()')
#
#     def setup_method(self, method):
#         print ('\nsetup_method()')
#
#     def teardown_method(self, method):
#         print ('\nteardown_method()')
#
#     def test_first(self):
#         print('test_first()')
#
#     def test_second(self):
#         print('test_second()')
#

import pytest

#
# @pytest.fixture()
# def resource_setup(request):
#     print("resource_setup")
#
#     def resource_teardown():
#         print("resource_teardown")
#
#     request.addfinalizer(resource_teardown)
#
#
# def test_1_that_needs_resource(resource_setup):
#     print("test_1_that_needs_resource")
#
#
# def test_2_that_does_not():
#     print("test_2_that_does_not")
#
#
# def test_3_that_does_again(resource_setup):
#     print("test_3_that_does_again")


# def test_simple():
#     one = 1
#     one_str = '1'
#     assert one_str == one
#
#
# import smtplib
# import pytest
#
#
# @pytest.fixture(scope="module")
# def smtp(request):
#     smtp = smtplib.SMTP("smtp.gmail.com")
#     def fin():
#         smtp.close()
#     request.addfinalizer(fin)
#     return smtp
#
#
# def test_ehlo(smtp):
#     response, msg = smtp.ehlo()
#     assert response == 250
#     assert b"smtp.gmail.com" in msg
#
#
# def test_noop(smtp):
#     response, msg = smtp.noop()
#     assert response == 250
#


