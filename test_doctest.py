import itertools
import doctest

'''
python3 -m doctest -v test_testing.py
'''

''' Важно doctest, сравнивает строковый вывод, по этому ожидаемый вывод который пишется в документальной строке должен 
быть идентичен выхлопу в терминале питона с строкой приглашения.(>>>)'''

def rle(iterable):
    '''Applet run-length encoding to an iterable'''
    for item, g in itertools.groupby(iterable):
        yield item, sum(1 for _ in g)


# list(rle('mississippi'))  # [('m', 1), ('i', 1), ('s', 2), ('i', 1), ('s', 2), ('i', 1), ('p', 2), ('i', 1)]
# print(list(rle('mississippi')))  # [('m', 1), ('i', 1), ('s', 2), ('i', 1), ('s', 2), ('i', 1), ('p', 2), ('i', 1)]


''' Doctest.Тесты в строке документации.

Дерективы:
    ... # doctest: +NORMALIZE_WHITESPACE - нормальлизует пробельные символы перед сравнением.
    ... # doctest: +ELLIPSIS - позволяет использовать символ ... которое совподает с любой строкой.'''


def rle(iterable):
    '''Applet run-length encoding to an iterable

    >>> print(list(rle('')))
    []

    >>> list(rle('mississippi'))
    ... # doctest: +NORMALIZE_WHITESPACE
    [('m', 1), ('i', 1), ('s', 2), ('i', 1),
     ('s', 2), ('i', 1), ('p', 2), ('i', 1)]

     >>> list(rle('mississippi'))
     ... # doctest: +ELLIPSIS
     [('m', 1), ('i', 1), ('s', 2), ('i', 1),...]
    '''
    for item, g in itertools.groupby(iterable):
        yield item, sum(1 for _ in g)


''' Вызов testmod() из модуля doctest. 
Всё это ограждено if __name__ == "__main__": чтоб когда мы модуль импортируем тесты не прогоонялись.'''
if __name__ == "__main__":
    doctest.testmod()


