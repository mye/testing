import pytest


'''
SKIPPED (s): Тест был пропущен. Вы можете заставить pytest пропустить тест, используя декораторы @pytest.mark.skip() или pytest.mark.skipif()
'''


@pytest.mark.skip()
def test_passing0():
    assert (1, 2, 3) == (1, 2, 3)


'''xfail (x): Тест не должен был пройти, был запущен и провалился. Вы можете принудительно указать pytest, что тест должен завершиться неудачей,
 используя декоратор @pytest.mark.xfail()'''


@pytest.mark.xfail()
def test_passing1():
    assert (1, 2, 3) == (1, 2, 3)