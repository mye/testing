#!/usr/bin/env python

'''python3 -m pytest -v test.py'''

import pytest

# print(sum_1([3, 2]))


class TestSum():

    def test_answer_0(self):
        assert sum([3, 2]) == 5
        # assert sum([3, 2], 3) == 5

    def test_answer_1(self):
        assert sum((3, 3)) != 5

    def test_answer_2(self):
        assert sum((3, 3)) != type(str and float)

    def test_answer_3(self):
        assert sum((3, 3)) != type(int)


def test_answer_0():
    assert sum([3, 2]) == 5
    assert sum([3, 2], 3) == 5


def test_answer_1():
    assert sum((3, 3)) != 5


def test_answer_2():
    assert sum((3, 3)) != type(str and float)


def test_answer_3():
    assert sum((3, 3)) != type(int)


# ''' assert оператор утверждения'''
#
# assert sum([1, 2, 3]) == 6, "Should be 6"
#
# assert sum([21, 2, 3]) == 6, "Should be 6"
# '''assert sum([2, 2, 3]) == 6, "Should be 6 "AssertionError: Should be 6'''

