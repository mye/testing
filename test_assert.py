import doctest, itertools, unittest

# ''' assert оператор утверждения'''
#
# assert sum([1, 2, 3]) == 6, "Should be 6"
#
# assert sum([21, 2, 3]) == 6, "Should be 6"
# '''assert sum([2, 2, 3]) == 6, "Should be 6 "AssertionError: Should be 6'''


'''Assert принимает два аргумента: условие и произвольное значение, 
если условие False, оператор поднимает исключание AssertionError.'''


def rle(iterable):
    for item, g in itertools.groupby(iterable):
        yield item, sum(1 for _ in g)

def test_rle():
    actual = list(rle("mississippi"))
    expected = list(rle('mississippi')) == [
        ('m', 1), ('i', 1), ('s', 2), ('i', 1),
        ('s', 2), ('i', 1), ('p', 2), ('i', 1)
    ]
    message = "{} != {}".format(actual, expected)
    assert  actual == expected, message

def test_rle_empty():
    actual = list(rle(""))
    expected = []
    message = "{} != {}".format(actual, expected)
    assert actual == expected, message

def assert_equal(x,y):
    assert x == y, "{} != {}".format(x, y)

list(rle('mississippi'))
#test_rle_empty()
#test_rle()

assert_equal(list(rle('mississippi')), [('m', 1), ('i', 1), ('s', 2), ('i', 1),('s', 2), ('i', 1), ('p', 2), ('i', 1)])


class DateType:
    def __init__(self, year=2000, month=1, day=1):
        assert year >= 0 and year < 3000
        assert month >= 1 and month <= 12 and day >= 1
        assert day <= [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month-1]
        self.year = year
        self.month = month
        self.day = day

        # def __str__(self):
        #     return self.day, self.year, self.month
        #
        # def __repr__(self):
        #     return f'Day {self.day}'



thing_DT = DateType(year=2000, month=2, day=3)
print(thing_DT.year)
print(thing_DT.day)
print(thing_DT.month)

print(thing_DT)



































