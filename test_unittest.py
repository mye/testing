import unittest, itertools

'''python3 -m unittest -v - current directory'''

class TestDivision(unittest.TestCase):
    def test_integer_division(self):
        self.assertIs(10 / 5, 2.0)  # AssertionError: 2.0 is not 2.0

    def test_integer_division_1(self):
        self.assertIsNot(10 / 5, 2.0)

    def test_integer_division_2(self):
        self.assertIs(2.0, 2.0)


def rle(iterable):
    for item, g in itertools.groupby(iterable):
        yield item, sum(1 for _ in g)

class TestHomework(unittest.TestCase):
    def test_rle(self):
        self.assertEqual(list(rle('mississippi')), [('m', 1), ('i', 1), ('s', 2), ('i', 1),
                                                    ('s', 2), ('i', 1), ('p', 2), ('i', 1)])

    def test_rle_empty(self):
        self.assertEqual(list(rle('')), [])


list(rle('mississippi'))


'''
def rle(iterable):
    for item, g in itertools.groupby(iterable):
        yield item, sum(1 for _ in g)

class TestHomework(unittest.TestCase):
    def test_rle(self):
        self.assertEqual(list(rle('mississippi')), [('m', 1), ('i', 1), ('s', 2), ('i', 1),
                                                    ('s', 2), ('i', 1), ('p', 2), ('i', 1)])

    def test_rle_empty(self):
        self.assertEqual(list(rle('')), [])


list(rle('mississippi'))
'''

if __name__ == "__main__":
    unittest.main()


