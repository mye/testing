from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import page_object_pattern.users as users
import requests
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import pytest


def routes(group, user):
    url = 'https://www.youtube.com/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)

    response = requests.get(url)
    # print(f'response.status_code = {response.status_code}')
    assert 200 == response.status_code

    # assert "check for smoke" in driver.title, 'check for smoke for driver.title'
    assert "YouTube" in driver.title

    # logo = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//a[@id='logo']"))).get_attribute('title')
    # print(logo)

    try:
        assert users.get_user(user)["email"] and users.get_user(user)[
            "password"], '\nEnter mail or password in users_pom.py'
    except AssertionError as ae:
        print(ae)

    WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
        (By.XPATH,
         "//yt-formatted-string[@class='style-scope ytd-button-renderer style-blue-text size-default']"))).click()

    assert WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//input[@id='identifierId']")))

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//input[@id='identifierId']"))).send_keys(
        users.get_user(user)["email"])

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//div[@id='identifierNext']"))).click()

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//input[@type='password']"))).send_keys(
        users.get_user(user)["password"])

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//div[@id='passwordNext']"))).click()

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//input[@id='search']"))).send_keys(group)
    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//button[@id='search-icon-legacy']"))).click()

    exist_group = WebDriverWait(driver, 15).until(
        EC.visibility_of_element_located((By.XPATH, "//span[@class='style-scope ytd-channel-renderer']"))).text
    assert exist_group == group

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//span[@class='style-scope ytd-channel-renderer']"))).click()

    WebDriverWait(driver, 5).until(
        EC.visibility_of_element_located((By.XPATH, "//div[@class='tab-content style-scope paper-tab']"))).click()
    time.sleep(2)

    WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
        (By.XPATH, "//div[@class='scrollable style-scope paper-tabs']/paper-tab[3]"))).click()
    print(driver.current_url)

    count_of_playlist = len(WebDriverWait(driver, 5).until(
        EC.visibility_of_all_elements_located((By.XPATH, "//div[@id='items']/ytd-grid-playlist-renderer"))))
    # print(count_of_playlist)

    for p_list in range(count_of_playlist + 1):
        if p_list:
            # xpath_arg = f"//div[@id='items']//ytd-grid-playlist-renderer[{p_list}]"
            # name_of_playlist  = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, xpath_arg))).text
            # print(ppp)

            value = f"//div[@id='items']//ytd-grid-playlist-renderer[{p_list}]"
            WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, value))).click()

            count_of_movies = int(WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                (By.XPATH, "//yt-formatted-string[@class='index-message style-scope ytd-playlist-panel-renderer']"))
            ).text.split('/')[-1])

            print(f'count_of_movies - {count_of_movies}')

            for movie in range(count_of_movies + 1):
                if type(movie) == int:
                    print(movie)
                    try:
                        time.sleep(2)
                        WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                            (By.XPATH,
                             "//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-default-active']")))

                        # start_video_id = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//ytd-watch-flexy[@class='style-scope ytd-page-manager hide-skeleton']"))).get_attribute("video-id")
                        # print(f'start_video_id in block try:- {start_video_id}')
                        # current_video_id = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"//ytd-watch-flexy[@class='style-scope ytd-page-manager hide-skeleton']"))).get_attribute("video-id")
                        # print(f'video_id in block try:- {current_video_id}')

                        # if movie == 1:
                        #     WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-default-active']")))
                        # else:
                        #     if current_video_id == start_video_id:
                        #         time.sleep(2)
                        #     else:
                        #         WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-default-active']")))
                        # current_video_id = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"//ytd-watch-flexy[@class='style-scope ytd-page-manager hide-skeleton']"))).get_attribute("video-id")
                        # name_of_movie = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//yt-formatted-string[@class='style-scope ytd-video-primary-info-renderer']"))).text
                        # print(name_of_movie)
                        # WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH, "//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-default-active']")))
                        # WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-text']"))).click()
                        # print('The "like" is exists.')

                    except TimeoutException:

                        name_of_movie = WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                            (By.XPATH,
                             "//yt-formatted-string[@class='style-scope ytd-video-primary-info-renderer']"))).text
                        print('The "like" isn`t exists.')
                        print(name_of_movie)

                        WebDriverWait(driver, 5).until(
                            EC.visibility_of_element_located((By.XPATH,
                                                              "//yt-icon-button[@class='style-scope ytd-toggle-button-renderer style-text']"))).click()

                        WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                            (By.XPATH, "//a[@class='ytp-next-button ytp-button']"))).click()
                    else:
                        print('The "like" is exists, go to next iteration.')
                        WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
                            (By.XPATH, "//a[@class='ytp-next-button ytp-button']"))).click()

                        # else:
            print('переход на главную страницу')
            WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                (By.XPATH, "//yt-formatted-string[@class='style-scope ytd-video-owner-renderer']"))).click()
            WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='scrollable style-scope paper-tabs']/paper-tab[3]"))).click()

    driver.close()


if __name__ == "__main__":
    # routes('Олег Молчанов', user='mye')
    routes('Юрий Хованский', user='mye')
