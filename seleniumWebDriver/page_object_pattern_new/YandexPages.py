from BaseApp import BasePage
from selenium.webdriver.common.by import By


class YandexSeacrhLocators:
    # LOCATOR_YANDEX_SEARCH_FIELD = (By.ID, "text")
    # LOCATOR_YANDEX_SEARCH_BUTTON = (By.CLASS_NAME, "search2__button")
    # LOCATOR_YANDEX_NAVIGATION_BAR = (By.CSS_SELECTOR, ".service__name")

    LOCATOR_GOOGLE_SEARCH_FIELD = (By.XPATH, "//input[@class='gLFyf gsfi']")
    LOCATOR_GOOGLE_SEARCH_BUTTON = (By.XPATH, "//input[@class='gNO89b']")
    # LOCATOR_GOOGLE_NAVIGATION_BAR = (By.XPATH, "//div[@id='hdtbSum']")
    LOCATOR_GOOGLE_NAVIGATION_BAR = (By.XPATH, "//div[@id='hdtb-msb-vis']")


class SearchHelper(BasePage):

    def enter_word(self, word):
        # search_field = self.find_element(YandexSeacrhLocators.LOCATOR_YANDEX_SEARCH_FIELD)
        search_field = self.find_element(YandexSeacrhLocators.LOCATOR_GOOGLE_SEARCH_FIELD)
        search_field.click()
        search_field.send_keys(word)
        return search_field

    def click_on_the_search_button(self):
        # return self.find_element(YandexSeacrhLocators.LOCATOR_YANDEX_SEARCH_BUTTON,time=2).click()
        return self.find_element(YandexSeacrhLocators.LOCATOR_GOOGLE_SEARCH_BUTTON, time=2).click()

    def check_navigation_bar(self):
        # all_list = self.find_elements(YandexSeacrhLocators.LOCATOR_YANDEX_NAVIGATION_BAR,time=2)

        all_list = self.find_elements(YandexSeacrhLocators.LOCATOR_GOOGLE_NAVIGATION_BAR, time=2)

        # print(all_list)
        return all_list

        # nav_bar_menu = [x.text for x in all_list if len(x.text) > 0]
        # print(nav_bar_menu)
        # return nav_bar_menu
