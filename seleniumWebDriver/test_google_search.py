from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pytest


# @pytest.fixture()
# # def setup_module():
# def driver():
#     driver = webdriver.Firefox(executable_path=r'../../e2e/geckodriver')
#     driver.maximize_window()
#     driver.get("http://www.python.org")

# def setUp(self):
    # self.driver = webdriver.Firefox()
    # self.driver = webdriver.Firefox(executable_path=r'../../e2e/geckodriver')


def test_search_in_python_org(driver):

    # driver.get("http://www.python.org")
    # self.assertIn("Python", driver.title)

    # elem = driver.find_element_by_name("q")
    # elem.send_keys("pycon")
    # assert "No results found." not in driver.page_source
    assert 'python' in driver.title.lower()
    print(driver.title.lower())
    # elem.send_keys(Keys.RETURN)


# def tearDown(self):
#     self.driver.close()




# import unittest
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
#
#
# class PythonOrgSearch(unittest.TestCase):
#
#     def setUp(self):
#         self.driver = webdriver.Firefox()
#
#     def test_search_in_python_org(self):
#         driver = self.driver
#         driver.get("http://www.python.org")
#         self.assertIn("Python", driver.title)
#         elem = driver.find_element_by_name("q")
#         elem.send_keys("pycon")
#         assert "No results found." not in driver.page_source
#         elem.send_keys(Keys.RETURN)
#
#     def tearDown(self):
#         self.driver.close()
#
#
# if __name__ == "__main__":
#     unittest.main()