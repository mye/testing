import os
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import page_object_pattern.users as users
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
import requests
import time
import pytest


def dir_for_pic():
    if not os.path.exists('screenshot_failure_of_test'):
        os.makedirs('screenshot_failure_of_test')


def routes(group, user):
    url = 'http://www.youtube.com/'

    # options = Options()
    # options.set_headless()
    # assert options.headless  # Operating in headless mode
    # driver = Firefox(options=options)
    # driver.get(url)
    #
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)

    response = requests.get(url)
    print(f'response.status_code = {response.status_code}')
    assert 200 == response.status_code

    try:
        assert users.get_user(user)["email"] and users.get_user(user)[
            "password"], '\nEnter mail or password in users_pom.py'
    except AssertionError as ae:
        print(ae)

    try:
        print('start')
        logo = WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//a[@id='logo']"))).get_attribute('title')
        print(logo)
        assert "youtube" in driver.title.lower(), "youtube in driver.title"

        WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
            (By.XPATH,
             "//yt-formatted-string[@class='style-scope ytd-button-renderer style-blue-text size-default']"))).click()

        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//input[@id='identifierId']"))).send_keys(
            users.get_user(user)["email"])
        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//div[@id='identifierNext']"))).click()

        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//input[@type='password']"))).send_keys(
            users.get_user(user)["password"])
        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//div[@id='passwordNext']"))).click()

        exist_group = WebDriverWait(driver, 15).until(
            EC.visibility_of_element_located((By.XPATH, "//span[@class='style-scope ytd-channel-renderer']"))).text
        assert exist_group == group

        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//span[@class='style-scope ytd-channel-renderer']"))).click()

        WebDriverWait(driver, 5).until(
            EC.visibility_of_element_located((By.XPATH, "//div[@class='tab-content style-scope paper-tab']"))).click()
        time.sleep(2)

        WebDriverWait(driver, 5).until(EC.visibility_of_element_located(
            (By.XPATH, "//div[@class='scrollable style-scope paper-tabs']/paper-tab[3]"))).click()
        print(driver.current_url)

        count_of_playlist = len(WebDriverWait(driver, 5).until(
            EC.visibility_of_all_elements_located((By.XPATH, "//div[@id='items']/ytd-grid-playlist-renderer"))))
        # print(count_of_playlist)


        print('finish')

    except Exception as flr:
        dir_for_pic()
        print(flr)
        current_crush = f'{flr}.png'.replace(' ', '_').lower()
        driver.save_screenshot(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), 'screenshot_failure_of_test', current_crush))
    finally:
        driver.quit()


if __name__ == "__main__":
    routes('Олег Молчанов', user='mye')
    # routes('Олег Молчанов', user='user_login_error')
    # routes('Олег Молчанов', user='user_password_error')
