from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains


class Page:
    def __init__(self, driver):
        self.driver = driver

    def find_element(self, *locator):
        return self.driver.find_element(*locator)




'''
class Page(object):
        def __init__(self, driver, WebDriverWait):
            self.driver = driver
            self.WebDriverWait = WebDriverWait

        def find_element(self, *locator):
            return self.WebDriverWait(self.driver, 15).until(EC.visibility_of_element_located(*locator))
'''
