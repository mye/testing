from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from base import Page
from locators import *
import users
import time
import requests


class LoginPage(Page, PageLocatars):

    def __init__(self, driver):
        self.locator = PageLocatars
        super().__init__(driver)

    def check_login_and_password(self, user):
        try:
            assert users.get_user(user)["email"] and users.get_user(user)[
                "password"], '\nEnter mail or password in users_pom.py'
        except AssertionError as ae:
            print(ae)

    def enter_email(self, user):
        self.find_element(*self.locator.EMAIL).send_keys(users.get_user(user)["email"])

    def enter_password(self, user):
        self.find_element(*self.locator.PASSWORD).send_keys(users.get_user(user)["password"])

    def click_login_button(self):
        self.find_element(*self.locator.MAIL_BUTTON_NEXT).click()

    def click_password_button(self):
        self.find_element(*self.locator.PASSWORD_BUTTON_NEXT).click()

    def login_mail(self, user):
        self.enter_email(user)
        self.click_login_button()

    def login_password(self, user):
        time.sleep(2)
        self.enter_password(user)
        self.click_password_button()

    def login_with_valid_user(self, user):
        self.check_login_and_password(user)
        self.login_mail(user)
        self.login_password(user)

    def login_with_invalid_user(self, user):
        self.check_login_and_password(user)
        self.login_mail(user)


    def check_page_title(self):
        return self.driver.title

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.LOGO) else False


    def check_logo_user_on_main_page(self):
        # return True if self.find_element(*self.locator.BUTTON_OF_ACCOUNT) else False
        return True if self.find_element(*self.locator.PASSWORD) else False


    def check_sign_in_page_loaded(self):
        return self.find_element(*self.locator.LOGO_SIGN_IN_PAGE).text

    def click_sign_in_button(self):
        self.find_element(*self.locator.SUBMIT).click()


# class LoginPageFailed(Page, PageLocatars):
#     def __init__(self, driver):
#         self.locator = MainPageLocatars
#         super().__init__(driver)



















'''
class MainPage(Page):
    def __init__(self, driver):
        self.locator = MainPageLocatars
        super().__init__(driver)  # Python3 version

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.LOGO) else False

    def search_item(self, item):
        self.find_element(*self.locator.SEARCH).send_keys(item)
        self.find_element(*self.locator.SEARCH).send_keys(Keys.ENTER)
        return self.find_element(*self.locator.SEARCH_LIST).text
        
    def click_sign_up_button(self):
        self.hover(*self.locator.ACCOUNT)
        self.find_element(*self.locator.SIGNUP).click()
        return SignUpPage(self.driver)

    def click_sign_in_button(self):
        self.hover(*self.locator.ACCOUNT)
        self.find_element(*self.locator.LOGIN).click()
        return LoginPage(self.driver)
'''