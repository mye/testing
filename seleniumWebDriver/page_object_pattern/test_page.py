from pages import *
from locators import *
from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options


class TestPage:

    @classmethod
    def setup_class(self):
        # self.driver = webdriver.Firefox()
        # self.driver.get('https://www.youtube.com/')
        # self.driver.implicitly_wait(15)
        # self.driver.maximize_window()

        self.url = 'https://www.youtube.com/'
        self.options = Options()
        # self.options.set_headless()
        self.options.headless = True
        assert self.options.headless, 'Operating in headless mode'
        self.driver = Firefox(options=self.options)
        self.driver.get(self.url)

    def test_page_load(self):
        page = LoginPage(self.driver)
        assert page.check_page_loaded()

    def test_page_title(self):
        page = LoginPage(self.driver)
        assert 'youtube' in page.check_page_title().lower()

    def test_negative_page_title(self):
        page = LoginPage(self.driver)
        assert 'mail' in page.check_page_title().lower()

    def test_sign_in_page(self):
        page = LoginPage(self.driver)
        page.click_sign_in_button()
        assert 'sign in' in page.check_sign_in_page_loaded().lower()

    def test_negative_sign_in_page(self):
        page = LoginPage(self.driver)
        page.click_sign_in_button()
        assert 'out' in page.check_sign_in_page_loaded().lower()

    def test_sign_in(self):
        page = LoginPage(self.driver)
        page.login_with_valid_user("mye")
        assert page.check_logo_user_on_main_page

    def test_sign_in_with_invalid_user(self):
        mainPage = LoginPage(self.driver)
        loginPage = mainPage.click_sign_in_button()
        # result = mainPage.login_with_valid_user("user_login_error")
        mainPage.login_with_invalid_user("user_login_error")
        # assert loginPage.check_logo_user_on_main_page
        assert mainPage.check_logo_user_on_main_page



    '''
     def test_sign_in_with_valid_user(self):
            mainPage = MainPage(self.driver)
            loginPage = mainPage.click_sign_in_button()
            result = loginPage.login_with_valid_user("valid_user")
            self.assertIn("yourstore/home", result.get_url())    
    '''






















        # @classmethod
    # def teardown_class(self):
    #     self.driver.close()


# class TestPageNegative():
#     @classmethod
#     def setup_class(self):
#         self.driver = webdriver.Firefox()
#         self.driver.get('https://www.youtube.com/')
#         self.driver.implicitly_wait(15)
#         self.driver.maximize_window()






    # def test_sign_in_with_invalid_password(self):
    #     page = LoginPage(self.driver)
    #     page.login_with_valid_user("user_password_error")
    #     assert page.check_logo_user_on_main_page

# @classmethod
    # def teardown_class(self):
    #     self.driver.close()




'''
class TestPage(CoreDriver):

    @classmethod
    def setup_class(self):
        self.driver = webdriver.Firefox()
        # self.driver.get("http://www.vk.com")
        self.driver.get("http://www.vk.com")
        self.driver.implicitly_wait(15)
        # self.driver.maximize_window()

    def test_page_load(self):
        page = LoginPage(self.driver)
        assert page.is_title_matches()

    def test_page_load_sign_in(self):
        page = LoginPage(self.driver)
        page.login_with_valid_user("mye")
        assert page.check_page_loaded()

    def test_sign_in(self):
        page = LoginPage(self.driver)
        page.login_with_valid_user("mye")
        assert page.check_page_loaded()

    @classmethod
    def teardown_class(self):
        self.driver.close()

# content of test_appsetup.py python3 -m pytest -s -v

import pytest

class App(object):
    def __init__(self, smtp_connection):
        self.smtp_connection = smtp_connection

@pytest.fixture(scope="module")
def app(smtp_connection):
    return App(smtp_connection)

def test_smtp_connection_exists(app):
    assert app.smtp_connection

@pytest.fixture()
def get_driver(request):
    # driver = webdriver.Chrome(executable_path=driver_path, service_log_path=log_path)
    driver = webdriver.Firefox()
    def close_driver():
        driver.quit()
    request.addfinalizer(close_driver)
    return driver

'''


#             # def is_title_matches(self):
#         #     """Verifies that the hardcoded text "Python" appears in page title"""
#         #     return "Python" in self.driver.title
#
#         # assert main_page.is_title_matches(), "python.org title doesn't match."
#
#     # def test_search_in_python_org(self):
#     #
#     #     """
#     #     Tests python.org search feature. Searches for the word "pycon" then verified that some results show up.
#     #     Note that it does not look for any particular text in search results page. This test verifies that
#     #     the results were not empty.
#     #     """
#     #
#     #     Load the main page. In this case the home page of Python.org.
#         # main_page = page.MainPage(self.driver)
#
#         # Checks if the word "Python" is in title
#         # assert main_page.is_title_matches(), "python.org title doesn't match."
#         # Sets the text of search textbox to "pycon"
#         # main_page.search_text_element = "pycon"
#         # main_page.click_go_button()
#         # search_results_page = page.SearchResultsPage(self.driver)
#         # Verifies that the results page is not empty
#         # assert search_results_page.is_results_found(), "No results found."
#
#     # def tearDown(self):
#     #     self.driver.close()
#
#
#         # assert search_results_page.is_results_found(), "No results found."
#         # TOP_PROFILE = (By.XPATH, '//div[@class="top_profile_name"]')
#

# if __name__ == "__main__":
#     start_0 = TestPage()
    # start_0.test_sign_in()
    # start_0.test_sign_in()
