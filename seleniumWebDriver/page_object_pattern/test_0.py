from pages import *
import pytest
from locators import *
from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options


class TestPage:

    def setup_class(self):
        '''always executed first'''
        # self.driver = webdriver.Firefox()
        # self.driver.get('https://www.youtube.com/')
        # self.driver.implicitly_wait(15)
        # self.driver.maximize_window()

        self.url = 'https://www.youtube.com/'
        self.options = Options()
        self.options.headless = True
        # self.options.set_headless()
        assert self.options.headless, 'Operating in headless mode'
        self.driver = Firefox(options=self.options)
        self.driver.get(self.url)

    def teardown_class(self):
        '''always executed at the end.'''
        self.driver.close()
        # self.driver.quit()

    def test_page_load(self):
        page = LoginPage(self.driver)
        assert page.check_page_loaded()

    def test_page_title(self):
        page = LoginPage(self.driver)
        assert '1youtube' in page.check_page_title().lower()

    # def test_negative_page_title(self):
    #     page = LoginPage(self.driver)
    #     assert 'mail' in page.check_page_title().lower()

    # def test_sign_in_page(self):
    #     page = LoginPage(self.driver)
    #     page.click_sign_in_button()
    #     assert 'sign in' in page.check_sign_in_page_loaded().lower()

    # def test_negative_sign_in_page(self):
    #     page = LoginPage(self.driver)
    #     page.click_sign_in_button()
    #     assert 'out' in page.check_sign_in_page_loaded().lower()

    # def test_sign_in(self):
    #     page = LoginPage(self.driver)
    #     page.login_with_valid_user("mye")
    #     assert page.check_logo_user_on_main_page


    # def test_sign_in_with_invalid_user(self):
    #     mainPage = LoginPage(self.driver)
    #     loginPage = mainPage.click_sign_in_button()
    #     result = mainPage.login_with_valid_user("user_login_error")

    #     mainPage.login_with_invalid_user("user_login_error")
    #     assert loginPage.check_logo_user_on_main_page
    #     assert mainPage.check_logo_user_on_main_page


'''

    # except AssertionError as ass:
    # except TimeoutException as ass:
    except Exception as ass:
        print(ass)

        current_failure_of_test = f'{ass}.png'.replace(' ', '_')
        print(current_failure_of_test)
        driver.save_screenshot(os.path.join(os.path.dirname(os.path.realpath(__file__)), './screenshot_failure_of_test', current_failure_of_test))


'''