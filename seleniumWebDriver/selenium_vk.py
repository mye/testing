import os
from selenium.webdriver.common.keys import Keys
'''Класс Keys обеспечивает взаимодействие с командами клавиатуры, такими как RETURN, F1, ALT и т.д…'''
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from testing.seleniumWebDriver.page_object_pattern import users
import requests
import pytest


@pytest.fixture()
def driver():
    # url = 'http://www.youtube.com/'
    url = 'http://google.com/'

    # options = Options()
    # options.headless = True
    # driver = webdriver.Firefox(options=options, executable_path=r'../../e2e/geckodriver')
    # driver.get(url)
    # print("Headless Firefox Initialized")
    # assert options.headless  # Operating in headless mode
    # print(options.headless) # True
    # print(driver.title)
    # assert "No results found." not in driver.page_source
    # # print(driver.page_source)

    driver = webdriver.Firefox(executable_path=r'../../e2e/geckodriver')
    driver.maximize_window()
    driver.get(url)

    print(f'driver.title - {driver.title}')
    print(f'driver.title.lower() - {driver.title.lower()}')
    assert "No results found." not in driver.page_source
    assert 'google' in driver.title.lower()

    # driver.quit()

    # response = requests.get(url)
    # print(f'response.status_code = {response.status_code}')
    # assert response.status_code == 200





# '''Place the following functions below in order of their efficiency.
# They all take in a list of numbers between 0 and 1.
# The list can be quite long.
# An example input list would be [random.random() for i in range(100000)].
# How would you prove that your answer is correct? -  profiling?'''
#
#
# def f1(lIn):
#     l1 = sorted(lIn)
#     l2 = [i for i in l1 if i<0.5]
#     return [i*i for i in l2]
#
#
# def f2(lIn):
#     l1 = [i for i in lIn if i<0.5]
#     l2 = sorted(l1)
#     return [i*i for i in l2]
#
#
# def f3(lIn):
#     l1 = [i*i for i in lIn]
#     l2 = sorted(l1)
#     return [i for i in l1 if i<(0.5*0.5)]

























# def vk_routes(name_group, user):
#     url = 'https://www.vk.com'
#     driver = webdriver.Firefox()
#     driver.maximize_window()
#     driver.get(url)
#
#     # # options = Options()
#     # # options.set_headless()
#     # # assert options.headless, 'Operating in headless mode'
#     # # driver = webdriver.Firefox(options=options)
#     # # driver.get(url)
#
#     response = requests.get(url)
#     print(f'response.status_code = {response.status_code}')
#     assert 200 == response.status_code
#
#     assert "VK" in driver.title
#
#     try:
#         assert users.get_user(user)["email"] and users.get_user(user)[
#             "password"], '\nEnter mail or password in users_pom.py'
#     except AssertionError as ae:
#         print(ae)
#
#     try:
#         WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.ID, "index_email"))).send_keys(
#             users.get_user(user)["email"])
#         WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.ID, "index_pass"))).send_keys(
#             users.get_user(user)["password"])
#         driver.find_element_by_id("index_login_button").click()
#         print(driver.title)
#
#         top_profile_name = WebDriverWait(driver, 15).until(
#             EC.visibility_of_element_located((By.XPATH, "//div[@class='top_profile_name']")))
#         assert top_profile_name
#
#         WebDriverWait(driver, 15).until(
#             EC.visibility_of_element_located((By.XPATH, '//input[@aria-label="Поиск"]'))).send_keys(name_group,
#                                                                                                     Keys.RETURN)
#
#         WebDriverWait(driver, 15).until(
#             EC.visibility_of_element_located((By.XPATH, "//a[@id='ui_rmenu_communities']"))).click()
#         WebDriverWait(driver, 15).until(
#             EC.visibility_of_element_located((By.XPATH, "//div[@class='labeled title']"))).click()
#
#         assert name_group in driver.title
#
#         try:
#             WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
#                 (By.XPATH, "//div[@id='public_albums']//a[@class='module_header']"))).click()
#         except:
#             WebDriverWait(driver, 15).until(
#                 EC.visibility_of_element_located((By.XPATH, "//img[@class='page_avatar_img']"))).click()
#             WebDriverWait(driver, 15).until(
#                 EC.visibility_of_element_located((By.CSS_SELECTOR, '.pv_album_name'))).click()
#             WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
#                 (By.XPATH, "//div[@class='page_block_header_inner _header_inner']/a[2]"))).click()
#
#         WebDriverWait(driver, 15).until(
#             EC.visibility_of_element_located((By.XPATH, "//div[@class='photos_row ']"))).click()
#
#         print(f'In group - {name_group} all count of pictures - {count_of_pictures(driver)}')
#         take_likes(count_of_pictures(driver), driver)
#     except Exception as ass:
#         print(f'the group not found - {group}')
#         current_failure_of_test = f'{ass}.png'.replace(' ', '_')
#         driver.save_screenshot(os.path.join(os.path.dirname(os.path.realpath(__file__)), './screenshot_failure_of_test',
#                                             current_failure_of_test))
#     else:
#         driver.quit()
#
#
# def take_likes(new_number_of_files_in_album, driver):
#     for like in range(new_number_of_files_in_album):
#         print(f'like  - {like}')
#         if like:
#             try:
#                 WebDriverWait(driver, 5).until(
#                     EC.visibility_of_element_located((By.CSS_SELECTOR, '.active')))
#             except:
#                 WebDriverWait(driver, 5).until(
#                     EC.visibility_of_element_located((By.CSS_SELECTOR, '.like_btn'))).click()
#                 print(f'like isn`t exists - {like}')
#                 driver.find_element_by_xpath("//body").send_keys(Keys.RIGHT)
#             else:
#                 print(f'like is exists, go to next iteration - {like}')
#                 driver.find_element_by_xpath("//body").send_keys(Keys.RIGHT)
#     driver.close()
#
#
# def count_of_pictures(driver):
#     number_of_files_in_album = WebDriverWait(driver, 15).until(EC.visibility_of_element_located(
#         (By.XPATH, "//div[@id='photos_all_block']//span[@class='ui_crumb_count']"))).text
#     new_number_of_files_in_album = int(number_of_files_in_album) + 1
#     return new_number_of_files_in_album
#
#
# def group_touch(list_groups):
#     for group in list_groups:
#         try:
#             vk_routes(group, user='mye')
#         except Exception as ass:
#             print(f'the group not found - {group}')
#
#
# if __name__ == "__main__":
#     # vk_routes('Pythontutor', user='mye', driver=driver_0())
#     group_touch(list_groups=['Pythontutor', 'CUBE MUSIC'])
