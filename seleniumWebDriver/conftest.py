from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pytest


@pytest.fixture()
def some_data():
    """Return answer to ultimate question."""
    return 42


@pytest.fixture()
def driver():
    driver = webdriver.Firefox(executable_path=r'../../e2e/geckodriver')
    driver.maximize_window()
    # driver.get("http://www.python.org")


@pytest.fixture(scope="session")
def browser():
    ''' Параметр scope со значением session. Это означает что данная функция-фикстура
    будет исполнятся только 1 раз за тестовую сессию.'''
    # driver = webdriver.Chrome(executable_path="./chromedriver")
    driver = webdriver.Firefox(executable_path=r'../../e2e/geckodriver')
    yield driver
    driver.quit()
